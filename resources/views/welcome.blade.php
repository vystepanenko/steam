<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">

    <div class="content">
        <div class="title m-b-md">
            Test Task
            <hr>

            @auth
                <img src="{{Auth::user()->avatar}}">
                <p>{{Auth::user()->username}}</p>
                <p>ID: {{Auth::user()->steamid}}</p>
                <div class="links">
                    <a href="https://steamcommunity.com/profiles/{{Auth::user()->steamid}}">Link to Your profile</a>
                    <a href="steamlogout">Logout</a>
                </div>
                    @else
                        <a href="steamlogin">
                            <img src="http://community.edgecast.steamstatic.com/public/images/signinthroughsteam/sits_01.png">
                        </a>
                        @endauth

                </div>


        </div>
    </div>
</body>
</html>
